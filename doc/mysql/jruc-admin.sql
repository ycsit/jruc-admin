/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50538
Source Host           : localhost:3306
Source Database       : jruc-admin

Target Server Type    : MYSQL
Target Server Version : 50538
File Encoding         : 65001

Date: 2017-05-11 17:15:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for system_bug_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_bug_4s`;
CREATE TABLE `system_bug_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(111) DEFAULT NULL,
  `des` text,
  `type` varchar(2) DEFAULT NULL COMMENT '类别',
  `createDate` datetime DEFAULT NULL,
  `status` bigint(20) DEFAULT '1' COMMENT '1.待解决 2. 已处理 3.忽略',
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_bug_4s
-- ----------------------------

-- ----------------------------
-- Table structure for system_log_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_log_4s`;
CREATE TABLE `system_log_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `operation` varchar(2) DEFAULT '0' COMMENT '1.访问 2 登录 3.添加 4. 编辑 5. 删除',
  `from` varchar(255) DEFAULT NULL COMMENT '来源 url',
  `ip` varchar(22) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_EVENT_4s` (`uid`) USING BTREE,
  CONSTRAINT `system_log_4s_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `system_user_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1565 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_log_4s
-- ----------------------------

-- ----------------------------
-- Table structure for system_oauth2_client_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_oauth2_client_4s`;
CREATE TABLE `system_oauth2_client_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(100) DEFAULT NULL,
  `client_id` varchar(100) DEFAULT NULL,
  `client_secret` varchar(100) DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_oauth2_client_client_id_4s` (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_oauth2_client_4s
-- ----------------------------

-- ----------------------------
-- Table structure for system_oauth2_token_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_oauth2_token_4s`;
CREATE TABLE `system_oauth2_token_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `token` varchar(100) DEFAULT NULL,
  `expires` bigint(13) DEFAULT NULL,
  `client_id` varchar(55) DEFAULT NULL,
  `uid` varchar(55) DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `token` (`token`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_oauth2_token_4s
-- ----------------------------

-- ----------------------------
-- Table structure for system_res_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_res_4s`;
CREATE TABLE `system_res_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `name` varchar(111) DEFAULT NULL,
  `des` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `iconCls` varchar(255) DEFAULT 'am-icon-file',
  `seq` bigint(20) DEFAULT '1',
  `type` varchar(2) DEFAULT '2' COMMENT '1 功能 2 权限',
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_res_4s
-- ----------------------------
INSERT INTO `system_res_4s` VALUES ('0', null, 'ROOT', '根', '/', null, 'am-icon-file', '0', '1', '1', 'admin', '2016-10-18 16:23:15', '初始数据');
INSERT INTO `system_res_4s` VALUES ('1', '0', '系统管理', '系统管理', '#', null, 'am-icon-file', '1', '1', '1', 'admin', '2016-10-25 10:47:23', '修改资源');
INSERT INTO `system_res_4s` VALUES ('3', '1', '资源管理', '资源管理', '/system/res', null, 'am-icon-file', '1', '1', '1', 'admin', '2016-10-18 16:24:08', '添加资源');
INSERT INTO `system_res_4s` VALUES ('4', '1', '用户管理', '用户管理', '/system/user', null, 'am-icon-file', '2', '1', '1', 'admin', '2016-10-18 16:25:27', '添加资源');
INSERT INTO `system_res_4s` VALUES ('8', '1', '数据管理', '数据管理', '/system/data', null, 'am-icon-file', '8', '1', '1', 'admin', '2016-10-20 21:29:24', '修改资源');
INSERT INTO `system_res_4s` VALUES ('9', '1', '日志管理', '日志管理', '/system/log', null, 'am-icon-file', '10', '1', '1', 'admin', '2016-10-18 19:31:47', '添加资源');
INSERT INTO `system_res_4s` VALUES ('10', '1', '角色管理', '角色管理', '/system/role', null, 'am-icon-file', '3', '1', '1', 'admin', '2016-10-20 17:13:12', '添加资源');
INSERT INTO `system_res_4s` VALUES ('11', '1', '二级菜单测试', '二级菜单测试', '/system/res', null, 'am-icon-file', '99', '1', '1', 'admin', '2016-10-24 14:30:44', '添加资源');
INSERT INTO `system_res_4s` VALUES ('12', '11', '三级菜单测试', '三级菜单测试', '/system/res', null, 'am-icon-file', '99', '1', '1', 'admin', '2016-10-24 14:31:08', '添加资源');
INSERT INTO `system_res_4s` VALUES ('13', '1', '个人资料', '个人资料', '/system/user/profile', null, 'am-icon-file', '15', '1', '1', 'admin', '2016-10-25 16:16:00', '添加资源');
INSERT INTO `system_res_4s` VALUES ('14', '1', '修改密码', '修改密码', '/system/user/changepwd', null, 'am-icon-file', '20', '1', '1', 'admin', '2016-10-25 16:16:30', '添加资源');
INSERT INTO `system_res_4s` VALUES ('15', '0', '一级菜单测试', '一级菜单测试', '#', null, 'am-icon-file', '99', '1', '1', 'admin', '2016-10-26 10:37:35', '添加资源');
INSERT INTO `system_res_4s` VALUES ('16', '15', '测试', '测试', '#', null, 'am-icon-file', '99', '1', '1', 'admin', '2016-10-26 10:37:57', '添加资源');
INSERT INTO `system_res_4s` VALUES ('17', '1', '页面管理', '页面管理', '/system/page', null, 'am-icon-file', '7', '1', '1', 'admin', '2017-03-29 16:31:47', '添加资源');
INSERT INTO `system_res_4s` VALUES ('19', '1', '数据库监控', '数据库监控', '/system/monitor/druid', null, 'am-icon-file', '10', '1', '1', 'admin', '2017-05-11 15:56:10', '添加资源');
INSERT INTO `system_res_4s` VALUES ('20', '3', '资源管理-添加', '资源管理-添加', '/system/res/add', null, 'am-icon-file', '1', '2', '1', 'admin', '2017-05-11 16:21:35', '添加资源');
INSERT INTO `system_res_4s` VALUES ('21', '3', '资源管理-编辑', '资源管理-编辑', '/system/res/update', null, 'am-icon-file', '2', '2', '1', 'admin', '2017-05-11 16:24:08', '添加资源');
INSERT INTO `system_res_4s` VALUES ('22', '3', '资源管理-删除', '资源管理-删除', '/system/res/delete', null, 'am-icon-file', '3', '2', '1', 'admin', '2017-05-11 16:24:46', '添加资源');
INSERT INTO `system_res_4s` VALUES ('23', '4', '用户管理-新增', '用户管理-新增', '/system/user/add', null, 'am-icon-file', '1', '2', '1', 'admin', '2017-05-11 16:35:50', '添加资源');
INSERT INTO `system_res_4s` VALUES ('24', '4', '用户管理-编辑', '用户管理-编辑', '/system/user/update', null, 'am-icon-file', '2', '2', '1', 'admin', '2017-05-11 16:36:21', '添加资源');
INSERT INTO `system_res_4s` VALUES ('25', '4', '用户管理-删除', '用户管理-删除', '/system/user/delete', null, 'am-icon-file', '3', '2', '1', 'admin', '2017-05-11 16:36:45', '添加资源');
INSERT INTO `system_res_4s` VALUES ('26', '10', '角色管理-新增', '角色管理-新增', '/system/role/add', null, 'am-icon-file', '1', '2', '1', 'admin', '2017-05-11 16:37:14', '添加资源');
INSERT INTO `system_res_4s` VALUES ('27', '10', '角色管理-编辑', '角色管理-编辑', '/system/role/update', null, 'am-icon-file', '1', '2', '1', 'admin', '2017-05-11 16:37:59', '修改资源');
INSERT INTO `system_res_4s` VALUES ('28', '10', '角色管理-删除', '角色管理-删除', '/system/role/delete', null, 'am-icon-file', '3', '2', '1', 'admin', '2017-05-11 16:38:28', '添加资源');
UPDATE `system_res_4s` SET id = 0 where `name` = 'ROOT';

-- ----------------------------
-- Table structure for system_role_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_role_4s`;
CREATE TABLE `system_role_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `des` varchar(55) DEFAULT NULL,
  `seq` bigint(20) DEFAULT '1',
  `iconCls` varchar(55) DEFAULT 'status_online',
  `pid` bigint(20) DEFAULT '0',
  `createdate` datetime DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role_4s
-- ----------------------------
INSERT INTO `system_role_4s` VALUES ('1', '管理员', '管理员-系统最高权限拥有者', '1', 'status_online', '0', '2016-10-19 15:02:25', '1', 'admin', '2017-05-11 16:41:59', '更新角色');

-- ----------------------------
-- Table structure for system_role_res_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_role_res_4s`;
CREATE TABLE `system_role_res_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `res_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTEM_ROLE_RES_RES_ID_4s` (`res_id`) USING BTREE,
  KEY `FK_SYSTEM_ROLE_RES_ROLE_ID_4s` (`role_id`) USING BTREE,
  CONSTRAINT `system_role_res_4s_ibfk_1` FOREIGN KEY (`res_id`) REFERENCES `system_res_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `system_role_res_4s_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `system_role_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role_res_4s
-- ----------------------------
INSERT INTO `system_role_res_4s` VALUES ('209', '1', '1');
INSERT INTO `system_role_res_4s` VALUES ('210', '3', '1');
INSERT INTO `system_role_res_4s` VALUES ('211', '20', '1');
INSERT INTO `system_role_res_4s` VALUES ('212', '21', '1');
INSERT INTO `system_role_res_4s` VALUES ('213', '22', '1');
INSERT INTO `system_role_res_4s` VALUES ('214', '4', '1');
INSERT INTO `system_role_res_4s` VALUES ('215', '23', '1');
INSERT INTO `system_role_res_4s` VALUES ('216', '24', '1');
INSERT INTO `system_role_res_4s` VALUES ('217', '25', '1');
INSERT INTO `system_role_res_4s` VALUES ('218', '8', '1');
INSERT INTO `system_role_res_4s` VALUES ('219', '9', '1');
INSERT INTO `system_role_res_4s` VALUES ('220', '10', '1');
INSERT INTO `system_role_res_4s` VALUES ('221', '26', '1');
INSERT INTO `system_role_res_4s` VALUES ('222', '27', '1');
INSERT INTO `system_role_res_4s` VALUES ('223', '28', '1');
INSERT INTO `system_role_res_4s` VALUES ('224', '11', '1');
INSERT INTO `system_role_res_4s` VALUES ('225', '12', '1');
INSERT INTO `system_role_res_4s` VALUES ('226', '13', '1');
INSERT INTO `system_role_res_4s` VALUES ('227', '14', '1');
INSERT INTO `system_role_res_4s` VALUES ('228', '17', '1');
INSERT INTO `system_role_res_4s` VALUES ('229', '19', '1');
INSERT INTO `system_role_res_4s` VALUES ('230', '15', '1');
INSERT INTO `system_role_res_4s` VALUES ('231', '16', '1');

-- ----------------------------
-- Table structure for system_user_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_user_4s`;
CREATE TABLE `system_user_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `status` varchar(2) DEFAULT '1' COMMENT '#1 正常 2.封号 ',
  `icon` varchar(255) DEFAULT 'images/guest.jpg',
  `email` varchar(255) DEFAULT NULL,
  `createdate` datetime DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `salt2` varchar(55) DEFAULT NULL,
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user_4s
-- ----------------------------
INSERT INTO `system_user_4s` VALUES ('3', 'admin', 'f69e5f6d696488b8cefa53f1f72399b6', '0', 'images/guest.jpg', null, '2017-05-11 15:54:56', null, '8096e180685d48527db4d49b1f22cf4c', 'admin', '2017-05-11 16:06:56', '修改系统用户');

-- ----------------------------
-- Table structure for system_user_role_4s
-- ----------------------------
DROP TABLE IF EXISTS `system_user_role_4s`;
CREATE TABLE `system_user_role_4s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SYSTME_USER_ROLE_USER_ID_4s` (`user_id`) USING BTREE,
  KEY `FK_SYSTME_USER_ROLE_ROLE_ID_4s` (`role_id`) USING BTREE,
  CONSTRAINT `system_user_role_4s_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `system_role_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `system_user_role_4s_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `system_user_4s` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user_role_4s
-- ----------------------------
INSERT INTO `system_user_role_4s` VALUES ('4', '3', '1');

-- ----------------------------
-- Table structure for t_test_info
-- ----------------------------
DROP TABLE IF EXISTS `t_test_info`;
CREATE TABLE `t_test_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_test_info
-- ----------------------------
INSERT INTO `t_test_info` VALUES ('1', 'wj', '20', '2016-11-07 14:11:52');
INSERT INTO `t_test_info` VALUES ('2', 'mxt', '30', '2016-11-07 14:12:04');
INSERT INTO `t_test_info` VALUES ('3', 'ww', '40', '2016-11-07 14:12:12');

-- ----------------------------
-- Table structure for website_data
-- ----------------------------
DROP TABLE IF EXISTS `website_data`;
CREATE TABLE `website_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL COMMENT '代码',
  `codeDesc` varchar(200) DEFAULT NULL COMMENT '代码描述',
  `type` varchar(20) DEFAULT NULL COMMENT '类型代码',
  `typeDesc` varchar(200) DEFAULT NULL COMMENT '类型描述',
  `status` varchar(2) DEFAULT NULL COMMENT '状态 0-未生效 1-已生效',
  `orderNo` varchar(255) DEFAULT NULL COMMENT '排序',
  `createDate` datetime DEFAULT NULL,
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of website_data
-- ----------------------------
INSERT INTO `website_data` VALUES ('1', '0', '未启用', 'STATUS_COMMON', '通用状态', '1', '1', '2016-09-28 11:00:18', 'sys', '2016-09-28 11:00:18', '初始数据');
INSERT INTO `website_data` VALUES ('2', '1', '启用', 'STATUS_COMMON', '通用状态', '1', '2', '2016-09-28 11:00:56', 'sys', '2016-09-28 11:00:18', '初始数据');
INSERT INTO `website_data` VALUES ('3', '0', '正常', 'STATUS_USER4S', '系统用户状态', '1', '1', '2016-09-28 11:20:03', 'sys', '2016-09-28 11:20:03', '添加数据字典');
INSERT INTO `website_data` VALUES ('4', '2', '冻结', 'STATUS_USER4S', '系统用户状态', '1', '2', '2016-09-28 11:20:38', 'sys', '2016-09-28 11:20:38', '添加数据字典');
INSERT INTO `website_data` VALUES ('5', '1', '菜单', 'TYPE_RES4S', '资源类型1-菜单 2-功能', '1', '1', '2016-10-18 14:37:15', 'sys', '2016-10-18 14:37:15', '添加数据字典');
INSERT INTO `website_data` VALUES ('6', '2', '功能', 'TYPE_RES4S', '资源类型1-菜单 2-功能', '1', '2', '2016-10-18 14:38:15', 'sys', '2016-10-18 14:38:15', '添加数据字典');

-- ----------------------------
-- Table structure for website_page
-- ----------------------------
DROP TABLE IF EXISTS `website_page`;
CREATE TABLE `website_page` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no` varchar(40) NOT NULL COMMENT '用户编码',
  `name` varchar(40) NOT NULL COMMENT '页面名称',
  `desc` varchar(200) DEFAULT NULL COMMENT '页面描述',
  `content` text COMMENT '页面内容',
  `preContent` varchar(200) DEFAULT NULL COMMENT '预览内容',
  `createDate` datetime NOT NULL,
  `status` varchar(2) NOT NULL COMMENT '1-启用 0-未启用',
  `lastUpdAcct` varchar(20) NOT NULL,
  `lastUpdTime` varchar(20) NOT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of website_page
-- ----------------------------
INSERT INTO `website_page` VALUES ('3', 'admin', '简单页面', '简单页面', '<p><img src=\"http://img.baidu.com/hi/jx2/j_0001.gif\" /><img src=\"http://img.baidu.com/hi/jx2/j_0002.gif\" /></p>\n<p><br /></p>\n<p>这是个简单CMS页面</p>\n<p><br /></p>\n<p><img src=\"http://img.baidu.com/hi/jx2/j_0024.gif\" /></p>', '', '2017-05-11 16:49:24', '1', 'admin', '2017-05-11 16:49:24', '添加页面');

-- ----------------------------
-- Table structure for website_res
-- ----------------------------
DROP TABLE IF EXISTS `website_res`;
CREATE TABLE `website_res` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `iconCls` varchar(200) DEFAULT NULL,
  `orderNo` int(11) DEFAULT '1',
  `type` varchar(2) DEFAULT '2' COMMENT '1 功能 2 权限',
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `createDate` datetime DEFAULT NULL,
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of website_res
-- ----------------------------

-- ----------------------------
-- Table structure for website_role
-- ----------------------------
DROP TABLE IF EXISTS `website_role`;
CREATE TABLE `website_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  `orderNo` int(11) DEFAULT '1',
  `iconCls` varchar(55) DEFAULT 'status_online',
  `pid` int(11) DEFAULT '0',
  `status` varchar(2) DEFAULT '1' COMMENT '1-启用 0-未启用',
  `createDate` datetime DEFAULT NULL,
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of website_role
-- ----------------------------

-- ----------------------------
-- Table structure for website_user
-- ----------------------------
DROP TABLE IF EXISTS `website_user`;
CREATE TABLE `website_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no` varchar(20) NOT NULL COMMENT '用户编号',
  `name` varchar(55) DEFAULT NULL COMMENT '用户名称-登录用户名',
  `pwd` varchar(255) DEFAULT NULL COMMENT '密码',
  `status` varchar(2) DEFAULT '1' COMMENT '#1 正常 2.封号',
  `icon` varchar(255) DEFAULT 'images/guest.jpg',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `createdate` datetime DEFAULT NULL COMMENT '创建日期',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话号码',
  `salt2` varchar(55) DEFAULT NULL COMMENT 'salt',
  `lastUpdAcct` varchar(20) DEFAULT NULL,
  `lastUpdTime` varchar(20) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of website_user
-- ----------------------------

-- ----------------------------
-- Table structure for website_weixin_user
-- ----------------------------
DROP TABLE IF EXISTS `website_weixin_user`;
CREATE TABLE `website_weixin_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no` varchar(40) NOT NULL COMMENT '用户编码',
  `openId` varchar(40) NOT NULL,
  `nickName` varchar(100) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `headImgUrl` varchar(200) DEFAULT NULL,
  `privilege` varchar(200) DEFAULT NULL,
  `unionid` varchar(40) DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `status` varchar(2) NOT NULL,
  `lastUpdAcct` varchar(20) NOT NULL,
  `lastUpdTime` varchar(20) NOT NULL,
  `note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of website_weixin_user
-- ----------------------------
