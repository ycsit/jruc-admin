package app;

import java.util.HashMap;
import java.util.Map;

import net.dreamlu.controller.UeditorApiController;

import org.beetl.core.GroupTemplate;
import org.beetl.ext.jfinal3.JFinal3BeetlRenderFactory;

import com.baidu.ueditor.UeditorConfigKit;
import com.jfinal.captcha.CaptchaManager;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.plugin.quartz.QuartzPlugin;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.plugin.redis.RedisPlugin;
import com.jfinal.template.Engine;
import com.rlax.framework.config.AppBaseConfig;
import com.rlax.framework.interceptor.Log4sInterceptor;
import com.rlax.framework.interceptor.SessionMessageInterceptor;
import com.rlax.framework.plugin.beetl.function.BreadCrumbsFunction;
import com.rlax.framework.plugin.beetl.function.KeyValueListFunction;
import com.rlax.framework.plugin.beetl.function.MenuFunction;
import com.rlax.framework.plugin.beetl.function.RequestAttrFunction;
import com.rlax.framework.plugin.beetl.function.ShiroFunction;
import com.rlax.framework.plugin.beetl.function.TimeStampConvFunction;
import com.rlax.framework.plugin.beetl.function.ValueDescFunction;
import com.rlax.framework.plugin.reids.RedisCaptchaCache;
import com.rlax.framework.plugin.reids.activerecord.ActiveRecordCache;
import com.rlax.framework.plugin.shiro.SessionHandler;
import com.rlax.framework.plugin.shiro.ShiroInterceptor4s;
import com.rlax.framework.support.cache.CacheExtKit;
import com.rlax.framework.support.file.FileManager;
import com.rlax.framework.support.file.FileManagerKit;
import com.rlax.framework.support.file.oss.AliyunOssFileManager;
import com.rlax.framework.support.sms.SmsSender;
import com.rlax.framework.support.sms.SmsSenderKit;
import com.rlax.framework.support.sms.aliyun.AliyunMessageServiceSmsSender;
import com.rlax.framework.support.ueditor.AliyunOssUeditorFileManager;
import com.rlax.framework.support.xss.XssHandler;

/**
 * 主配置文件
 * @author Rlax
 *
 */
public class AppConfig extends AppBaseConfig {

	@Override
	public void afterJFinalStarted() {
		PropKit.use(cfg);
		
		/** 阿里云OSS初始化 */
	    String endpoint = PropKit.get("file.oss.endpoint");
        String accessId = PropKit.get("file.oss.accessId");
        String accessKey = PropKit.get("file.oss.accessKey");
        String bucket = PropKit.get("file.oss.bucket");
        FileManager fileManager = new AliyunOssFileManager(endpoint, accessId, accessKey, bucket);
        FileManagerKit.add(fileManager, "jruc-files");
        FileManagerKit.use("jruc-files");
        
        /** 阿里云 SMS 初始化 */
        accessId = PropKit.get("sms.ms.aliyun.accessId");
        accessKey = PropKit.get("sms.ms.aliyun.accessKey");
        String mnsEndpoint = PropKit.get("sms.ms.aliyun.mnsEndpoint"); 
        String topic = PropKit.get("sms.ms.aliyun.topic"); 
        SmsSender smsSender = new AliyunMessageServiceSmsSender(accessId, accessKey, mnsEndpoint, topic);
        SmsSenderKit.add(smsSender, "jruc-sms");
        SmsSenderKit.use("jruc-sms");
        
        /** Ueditor 初始化 */
	    endpoint = PropKit.get("file.ueditor.endpoint");
        accessId = PropKit.get("file.ueditor.accessId");
        accessKey = PropKit.get("file.ueditor.accessKey");
        bucket = PropKit.get("file.ueditor.bucket");
        
	    UeditorConfigKit.setFileManager(new AliyunOssUeditorFileManager(endpoint, accessId, accessKey, bucket));
	    
	    /** 系统使用 ehcache 缓存 */
	    //CacheExtKit.use(CacheExtKit.EHCACHE_TYPE);
	    
	    /** 系统使用 redis 缓存 */
	    //CacheExtKit.use(CacheExtKit.REDIS_TYPE);
	    
	    /** 系统使用 二级缓存 ehcache + redis */
	    CacheExtKit.use(CacheExtKit.L2_TYPE);
	    
	    /** 集群模式下验证码使用 redis 缓存 */
	    CaptchaManager.me().setCaptchaCache(new RedisCaptchaCache());
	}

	@Override
	public void configMoreConstants(Constants me) {
		me.setError404View("error404.html");
		me.setError500View("error500.html");
		me.setError403View("error500.html");
		
        JFinal3BeetlRenderFactory rf = new JFinal3BeetlRenderFactory();
        rf.config();
        me.setRenderFactory(rf);
		
		GroupTemplate groupTemplate = rf.groupTemplate;
		
		/** 模版全局变量 */
		Map<String, Object> sharedVars = new HashMap<String, Object>();
		sharedVars.put("system_name", APP_NAME);
		sharedVars.put("viewPath", "/WEB-INF/views");
		groupTemplate.setSharedVars(sharedVars);
		
		/** 模版注册函数 */
		groupTemplate.registerFunction("value", new RequestAttrFunction());
		groupTemplate.registerFunction("selectlist", new KeyValueListFunction());
		groupTemplate.registerFunction("valuedesc", new ValueDescFunction());
		groupTemplate.registerFunction("timestamp2str", new TimeStampConvFunction());
		groupTemplate.registerFunction("menu", new MenuFunction());
		groupTemplate.registerFunction("breadcrumbs", new BreadCrumbsFunction());
		groupTemplate.registerFunctionPackage("so", ShiroFunction.class);
	}

	@Override
	public void configMoreHandlers(Handlers me) {
		me.add(new DruidStatViewHandler("/druid"));
		me.add(new SessionHandler());
		me.add(new XssHandler("admin"));
	}

	@Override
	public void configMoreInterceptors(Interceptors me) {
		me.add(new SessionMessageInterceptor());
		me.add(new Log4sInterceptor());
		me.add(new ShiroInterceptor4s());
	}

	@Override
	public void configMorePlugins(Plugins me) {
		me.add(new EhCachePlugin());
		me.add(new QuartzPlugin("job.properties"));
		
		PropKit.use(cfg);
	    String name = PropKit.get("cache.redis.name");
        String host = PropKit.get("cache.redis.host");
        Integer port = PropKit.getInt("cache.redis.port");
        Integer timeout = PropKit.getInt("cache.redis.timeout");
        String password = PropKit.get("cache.redis.password");
		
		me.add(new RedisPlugin(name, host, port, timeout, password));
		
		/** 接入kisso 可配合 shiro 也可单独使用 */
		//me.add(new KissoJfinalPlugin());
	}

	@Override
	public void configMoreRoutes(Routes me) {
		me.setBaseViewPath("/WEB-INF/views");
		me.add("/ueditor/api", UeditorApiController.class);
	}

	@Override
	public void configTablesMapping(String ds, ActiveRecordPlugin arp) {
		arp.setCache(new ActiveRecordCache());
	}
	
	@Override
	public void configMoreEngines(Engine me) {
		
	}
	
	public static void main(String[] args) {
		JFinal.start("src/main/webapp", 8899, "/", 5);
	}

}
